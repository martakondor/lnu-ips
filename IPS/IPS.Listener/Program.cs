﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace IPS.Listener
{
	class Program
	{
		static void Main(string[] args)
		{ 
			RabbitMQListener.Receive();
        }
	}
}
