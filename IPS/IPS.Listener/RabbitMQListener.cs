﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace IPS.Listener
{
	public static class RabbitMQListener
	{
        public static void Receive()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine(" Listener has been started");
            string message = "";

            MongoDBConnection m = new MongoDBConnection();
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "queue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    message = Encoding.UTF8.GetString(body.ToArray());
                    if (message.Length > 0)
                    {
                        m.InsertData(message);
                    }


                    Console.WriteLine(" [x] Received {0}", message);
                };
                channel.BasicConsume(queue: "queue",
                                             autoAck: true,
                                             consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
	}
}
