﻿using IPS.DAL.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IPS.Listener
{
	class MongoDBConnection
	{
        MongoClient dbClient = new MongoClient();
        IMongoCollection<BsonDocument> collection;
        IMongoDatabase database;

        public MongoDBConnection()
        {
            var connectionString = "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false";
            var client = new MongoClient(connectionString);
            database = client.GetDatabase("IPS");
            collection = database.GetCollection<BsonDocument>("News");
            Console.WriteLine("Mongo db is connected");
        }

        public void InsertData(string data)
        {
            List<NewsModel> infos = JsonConvert.DeserializeObject<List<NewsModel>>(data);

            foreach (var item in infos)
            {
                var document = new BsonDocument() {
                    {"Author", item.Author },
                    {"Title", item.Title },
                    {"Content", item.Content }
                };
                collection.InsertOne(document);
            }
        }
    }
}
