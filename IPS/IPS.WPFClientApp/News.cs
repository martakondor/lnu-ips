﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IPS.WPFClientApp
{
	public class News
	{
		public string Id { get; set; }
		public string Author { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public bool Like { get; set; }

		public override string ToString()
		{
			return string.Format(">>> {0} <<<{1}{2} {1}Author: {3}",
				Title,
				Environment.NewLine,
				Content,
				Author);
		}
	}
}
