﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IPS.WPFClientApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>

	public partial class MainWindow : Window
	{
		private readonly NewsService _postService = new NewsService();
		private List<News> _posts = new List<News>();

		public MainWindow()
		{
			InitializeComponent();
			_posts = _postService.GetAllPost();
			UpdateListView();
		}

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
			var button = (sender as Button);
			var id = button.Tag.ToString();
			var post = _posts.FirstOrDefault(x => x.Id == id);

			await _postService.FeedbackAsync(id, post);
			_posts.FirstOrDefault(x => x.Id == id).Like = post.Like;

			button.Background = post.Like ? Brushes.GreenYellow : Brushes.LightPink;
			button.Name = post.Like ? "Like" : "Dislike";
			button.Content = post.Like ? "Like" : "Dislike";
		}

		private void UpdateListView()
		{
			this.postsList.Items.Clear();
			foreach (var post in _posts)
			{
				this.postsList.Items.Add(GetPostData(post));
				var title = post.Like ? "Like" : "Dislike";
				var color = post.Like ? Brushes.GreenYellow : Brushes.LightPink;
				Button likeButton = new Button() { Name = title, Width = 50, Height = 20, Content = title, Tag = post.Id, Background = color };
				likeButton.Click += new RoutedEventHandler(Button_Click);
				this.postsList.Items.Add(likeButton);
			}
		}

		private string GetPostData(News post)
		{
			return post.ToString();
		}
	}
		
}
