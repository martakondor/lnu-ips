﻿using GraphQL.Client;
using GraphQL.Common.Request;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IPS.WPFClientApp
{
	public class NewsService
	{
		protected readonly GraphQLClient _client;
		protected readonly string _url = "https://localhost:44394/graphql";

		public NewsService()
		{
			_client = new GraphQLClient(_url);
		}

        public List<News> GetAllPost()
        {
            var request = new GraphQLRequest()
            {
                Query = @"
                    query {
                        getAllNews {
                          id
                          author
                          content
                          title
                          like
                        }
                      }"
            };
            var graphQLResponse = _client.PostAsync(request).Result;
            var posts = graphQLResponse.GetDataFieldAs<List<News>>("getAllNews");
            return posts;
        }

        public async Task<bool> FeedbackAsync(string id, News updatedNews)
        {
            updatedNews.Like = !updatedNews.Like;
            var request = new GraphQLRequest()
            {
                Query = @"
                    mutation($news: NewsInput!, $newsId: ID!) {
                    likeNews(news: $news, newId: $newsId) {
                      id
                      author
                      title
                      content
                      like
                    }
                  }",
                Variables = new { news = updatedNews, newsId = id }
            };
            var graphQLResponse = await _client.PostAsync(request);
            var upPost = graphQLResponse.GetDataFieldAs<News>("likeNews");
            return upPost.Like;
        }


    }
}
