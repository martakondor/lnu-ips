

using IPS.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using AutoMapper;
using IPS.WebAPI.MappingProfiles;
using IPS.WebAPI.Query;
using IPS.WebAPI.Query.Type;
using GraphQL;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using GraphQL.Types;
using GraphiQl;

namespace IPS.WebAPI
{
	public class Startup
	{

		public IConfiguration Configuration { get; set; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services)
		{

			//services.AddSingleton<NewsQuery>();

			services.AddSingleton<NewsType>();
			services.AddSingleton<NewsInputType>();

			services.AddScoped<IDependencyResolver>(s => new
					FuncDependencyResolver(s.GetRequiredService));
			services.AddScoped<NewsSchema>();
			services.AddGraphQL().AddGraphTypes(ServiceLifetime.Scoped);

			services.AddCors(o => o.AddPolicy("WebApiPolicy", builder =>
			{
				builder.AllowAnyOrigin()
					   .AllowAnyMethod()
					   .AllowAnyHeader();
			}));

		

			var config = new MongoConnection();

			Configuration.GetSection("MongoConnection").Bind(config);

			//services.AddScoped<IRepository>(_ => new Repository(config, mapperConfig.CreateMapper()));

			services.AddMvc(option => option.EnableEndpointRouting = false);

			var mappingConfig = new MapperConfiguration(mc =>
			{
				mc.AddProfile(new MappingProfile());
			});

			services.Configure<IISServerOptions>(options =>
			{
				options.AllowSynchronousIO = true;
			});

			IMapper mapper = mappingConfig.CreateMapper();
			services.AddSingleton(mapper);
			services.AddScoped<IRepository>(_ => new Repository(config, mapper));
			var spp = services.BuildServiceProvider();
			services.AddSingleton<ISchema>(new NewsSchema(new FuncDependencyResolver(type => spp.GetService(type))));
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseCors("WebApiPolicy");

			app.UseHttpsRedirection();
			app.UseMvc();

			app.UseGraphQL<NewsSchema>();
			app.UseGraphiQl("/graphql");
			app.UseGraphQLPlayground
			   (new GraphQLPlaygroundOptions());
		}
	}
}
