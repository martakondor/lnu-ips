﻿using IPS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.Interfaces
{
	public interface IScraper
	{
		IEnumerable<NewsModel> CollectInfo();
	}
}
