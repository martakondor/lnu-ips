﻿using GraphQL.Types;
using IPS.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.Query
{
	class NewsType : ObjectGraphType<News>
	{
		public NewsType()
		{
			Field(x => x.Id);
			Field(x => x.Author);
			Field(x => x.Title);
			Field(x => x.Content);
			Field(x => x.Like);
		}
	}
	
}
