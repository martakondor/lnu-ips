﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.Query.Type
{
	public class NewsInputType: InputObjectGraphType
    {
        public NewsInputType()
        {
            Name = "NewsInput";
            Field<NonNullGraphType<StringGraphType>>("id");
            Field<StringGraphType>("author");
            Field<StringGraphType>("title");
            Field<StringGraphType>("content");
            Field<BooleanGraphType>("like");
        }
    }
}
