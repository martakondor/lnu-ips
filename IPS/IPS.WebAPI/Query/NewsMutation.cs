﻿using GraphQL.Types;
using IPS.DAL;
using IPS.DAL.Entity;
using IPS.WebAPI.Query.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.Query
{
	public class NewsMutation: ObjectGraphType
	{
		public NewsMutation(IRepository repository)
		{
			Field<NewsType>("likeNews",
				arguments: new QueryArguments(new QueryArgument<NonNullGraphType<NewsInputType>> { Name = "news" },
						   new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "newId" }),
				resolve: context =>
				{
					var news = context.GetArgument<News>("news");
					var newsId = context.GetArgument<string>("newId");
					var dbOwner = repository.GetById(newsId);
					return repository.Update(newsId, news);
				});
		}
	}
}
