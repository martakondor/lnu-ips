﻿using GraphQL.Types;
using IPS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.Query
{
	public class NewsQuery: ObjectGraphType
	{
		public NewsQuery(IRepository repository)
		{
			Field<ListGraphType<NewsType>>("getAllNews", resolve: context => repository.GetAllAsync().GetAwaiter().GetResult());

			Field<NewsType>("getNewsById",
				arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
				resolve: context =>
				{
					var id = context.GetArgument<string>("id");
					return repository.GetAllAsync().GetAwaiter().GetResult().FirstOrDefault(x => x.Id == id);
				});
		}
	}
}
