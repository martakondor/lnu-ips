﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.Query
{
	public class NewsSchema: Schema
	{
		public NewsSchema(IDependencyResolver resolver) : base(resolver)
		{
			Query = resolver.Resolve<NewsQuery>();
			Mutation = resolver.Resolve<NewsMutation>();
		}
	}
}
