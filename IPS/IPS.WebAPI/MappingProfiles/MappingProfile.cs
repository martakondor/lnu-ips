﻿using AutoMapper;
using IPS.DAL.Entity;
using IPS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPS.WebAPI.MappingProfiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<News, NewsModel>();
            CreateMap<NewsModel, News>();
        }
    }
}
