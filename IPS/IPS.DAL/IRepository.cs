﻿using IPS.DAL.Entity;
using IPS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace IPS.DAL
{
	public interface IRepository
	{
        Task<IEnumerable<News>> GetAllAsync();

        News GetById(string id);

        Task AddAsync(NewsModel item);

        Task AddAsync(List<NewsModel> items);

        News Update(string id, News newsUpd);
    }
}
