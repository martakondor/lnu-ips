﻿using IPS.DAL.Entity;
using MongoDB.Driver;


namespace IPS.DAL
{
    public class MongoDBContext
    {
        private readonly IMongoDatabase _database;

        public MongoDBContext(MongoConnection config)
        {
            var client = new MongoClient(config.ConnectionString);

            _database = client?.GetDatabase(config.Database);
        }


        public IMongoCollection<News> News => _database.GetCollection<News>("News");
        //public IMongoCollection<Article> Articles => _database.GetCollection<Article>("Articles");
    }
}