﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IPS.DAL.Entity
{
	public class News
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		public string Title { get; set; }

		public string Content { get; set; }

		public string Author { get; set; }

		public bool Like { get; set; }
	}
}
