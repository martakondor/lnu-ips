﻿namespace IPS.DAL.Models
{
	public class NewsModel
	{
		public string Title { get; set; }

		public string Content { get; set; }

		public string Author { get; set; }	
	}
}
