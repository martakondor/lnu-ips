﻿using AutoMapper;
using IPS.DAL.Entity;
using IPS.DAL.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IPS.DAL
{
	public class Repository: IRepository
	{
        private readonly MongoDBContext _context = null;
        private readonly IMapper _mapper;

        public Repository(MongoConnection config, IMapper mapper)
        {
            _context = new MongoDBContext(config);
            _mapper = mapper;
        }

        public async Task<IEnumerable<News>> GetAllAsync() 
        {
            return await _context.News.Find(_ => true).ToListAsync();
        }
        public News GetById(string id)
        {
            return _context.News.Find(x => x.Id == id).FirstOrDefault();
        }

        public async Task AddAsync(NewsModel item)
        {
            try
            {
                await _context.News.InsertOneAsync(_mapper.Map<News>(item));
            }
            catch (Exception)
            { }
        }

        public async Task AddAsync(List<NewsModel> items)
        {
            try
            {
                await _context.News.InsertManyAsync(_mapper.Map<IEnumerable<News>>(items));
            }
            catch (Exception)
            { }
        }

        public News Update(string id, News newsUpd)
        {
            _context.News.ReplaceOne(x => x.Id == id, newsUpd);
            return newsUpd;
        }

        
        //public async Task<ArticleModel> UpdateAsync((Guid guid, bool? status) request)
        //{
        //    //var filter = Builders<GismeteoEntity>.Filter.Eq("GismeteoGuid", request.guid.ToString());
        //    //var update = Builders<GismeteoEntity>.Update.Set("WeatherLike", request.status);

        //    await _context.Article.UpdateOneAsync(filter, update);

        //    return _mapper.Map<GismeteoTemperature>(await _context.Article.Find(x => x.GismeteoGuid == request.guid).FirstOrDefaultAsync());
        //}
    }
}
