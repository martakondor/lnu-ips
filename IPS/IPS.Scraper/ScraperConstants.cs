﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IPS.Scraper
{
	public  class ScraperConstants
	{
        public  string Url => @"https://www.the-village.com.ua/news";

        public  int TimeOutValue => 5000;

        public  string NewsSelector => "post-item-news";
        public string PostLinkSelector => "post-link";

        public string ContentSelector => "article-text";

        public string TitleSelector => "article-title";

        public string AuthorSelector => "meta-posted";

        public string LinkSelector = "a";
    }
}
