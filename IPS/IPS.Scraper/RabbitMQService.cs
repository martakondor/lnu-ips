﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace IPS.Scraper
{
    public class RabbitMQService
    {
        public static void Send(string data)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            factory.RequestedHeartbeat = new TimeSpan(60);
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "queue",
                                        durable: false,
                                        exclusive: false,
                                        autoDelete: false,
                                        arguments: null);

                var body = Encoding.UTF8.GetBytes(data);

                channel.BasicPublish(exchange: "",
                                        routingKey: "queue",
                                        basicProperties: null,
                                        body: body);
                Console.WriteLine(" [x] Sent {0}", data);

            }
        }
    }
}
