﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace IPS.Scraper
{
	class Program
	{
		public static IWebDriver _driver;
		public static ScraperConstants _constants;

		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;

			string data = CollectInfo();
			RabbitMQService.Send(data);
			_driver.Quit();
		}

		public static string CollectInfo()
		{
			_constants = new ScraperConstants();
			_driver = new ChromeDriver();
			_driver.Url = _constants.Url;

			string jsonResult = "";
			var news = new List<NewsModel>();

			_driver.Navigate().GoToUrl(_constants.Url);

			var newsList = _driver.FindElements(By.ClassName(_constants.PostLinkSelector)).Select(x=>x.GetAttribute("href")).ToList();

			for(int i = 0; i < 1; i++)
			{
				_driver.Navigate().GoToUrl(newsList[i]);

				string a = _driver.FindElement(By.ClassName(_constants.TitleSelector)).Text;
				string b = _driver.FindElement(By.ClassName(_constants.AuthorSelector)).FindElement(By.TagName(_constants.LinkSelector)).Text;
				string c = _driver.FindElement(By.ClassName(_constants.ContentSelector)).Text.Replace("\r\n", " ");
				
				news.Add(new NewsModel
				{
					Title = _driver.FindElement(By.ClassName(_constants.TitleSelector)).Text,
					Author = _driver.FindElement(By.ClassName(_constants.AuthorSelector)).FindElement(By.TagName(_constants.LinkSelector)).Text,
					Content = _driver.FindElement(By.ClassName(_constants.ContentSelector)).Text//.Replace("\r\n", " ")
				});

			}

			if (news != null)
			{
				jsonResult = JsonConvert.SerializeObject(news, Formatting.Indented);
			}

			if (news.Any())
			{
				foreach (var item in news)
				{
					Console.WriteLine(item.Author + "\n" + item.Title + "\n\n" + item.Content);
					Console.WriteLine("_________________________________________________________________");
				}
			}

			return jsonResult;
		}
	}
}
