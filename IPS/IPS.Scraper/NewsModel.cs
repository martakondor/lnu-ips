﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IPS.Scraper
{
	class NewsModel
	{
		public string Title { get; set; }

		public string Content { get; set; }

		public string Author { get; set; }
	}
}
